package glaubernespoli.panono.code.challenge.service.impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.OffsetDateTime;
import java.time.ZoneId;

import glaubernespoli.panono.code.challenge.handler.relevant.data.cut.RelevantDataCutHandler;
import glaubernespoli.panono.code.challenge.pojo.Statistics;
import glaubernespoli.panono.code.challenge.repository.PanoramaRepository;
import glaubernespoli.panono.code.challenge.service.StatisticsService;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class StatisticsServiceImplTest {

	@Autowired
	private StatisticsService statisticsService;

	@MockBean
	private PanoramaRepository panoramaRepository;

	@MockBean
	private RelevantDataCutHandler relevantDataCutHandler;

	private final ZoneId userZoneId = ZoneId.of("UTC");

	private final OffsetDateTime cut = OffsetDateTime.now(userZoneId);

	private Statistics result;

	@Before
	public void setUp() {
		when(relevantDataCutHandler.getCut(any())).thenReturn(cut);
	}

	@Test
	public void whenHasRelevantData_thenReturnStatistics() {
		//given
		Statistics statistics = new Statistics(21L, 7.0, 12, 3, 3L);
		when(panoramaRepository.findByCreatedAtGreaterThan(cut)).thenReturn(statistics);

		//when
		result = statisticsService.getStatistics(userZoneId);

		//then
		assertEquals("Sum different from expected", new Long(21), result.getSum());
		assertEquals("Avg different from expected", new Double(7.0), result.getAvg());
		assertEquals("Max different from expected", new Integer(12), result.getMax());
		assertEquals("Min different from expected", new Integer(3), result.getMin());
		assertEquals("Count different from expected.", new Long(3), result.getCount());
	}

	@Test
	public void whenDoesntHaveRelevantData_thenReturnNoStatistics() {
		//given
		Statistics statistics = new Statistics(null, null, null, null, null);
		when(panoramaRepository.findByCreatedAtGreaterThan(cut)).thenReturn(statistics);

		//when
		result = statisticsService.getStatistics(userZoneId);

		//then
		assertEquals("Sum different from expected", new Long(0), result.getSum());
		assertEquals("Avg different from expected", new Double(0.0), result.getAvg());
		assertEquals("Max different from expected", new Integer(0), result.getMax());
		assertEquals("Min different from expected", new Integer(0), result.getMin());
		assertEquals("Count different from expected.", new Long(0), result.getCount());
	}

	@TestConfiguration
	static class StatisticsServiceImplTestContextConfiguration {

		@Bean
		public StatisticsService statisticsService() {
			return new StatisticsServiceImpl();
		}
	}

}