package glaubernespoli.panono.code.challenge.service.impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;

import glaubernespoli.panono.code.challenge.handler.relevant.data.cut.RelevantDataCutHandler;
import glaubernespoli.panono.code.challenge.handler.timestamp.instant.TimestampToInstantHandler;
import glaubernespoli.panono.code.challenge.pojo.PanoramaPojo;
import glaubernespoli.panono.code.challenge.repository.PanoramaRepository;
import glaubernespoli.panono.code.challenge.service.UploadService;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class UploadServiceImplTest {

	@Autowired
	private UploadService uploadService;

	@MockBean
	private PanoramaRepository panoramaRepository;

	@MockBean
	private RelevantDataCutHandler relevantDataCutHandler;

	@MockBean
	private TimestampToInstantHandler timestampToInstantHandler;

	private final ZoneId userZoneId = ZoneId.of("UTC");

	private OffsetDateTime cut = OffsetDateTime.now(userZoneId).minusMinutes(1).withNano(0);

	private HttpStatus result;

	@Before
	public void setUp() {
		when(relevantDataCutHandler.getCut(any())).thenReturn(cut);
	}

	@Test
	public void whenUploadedTimeBeforeCut_shouldNotUpload() {
		//given
		final long uploadTime = cut.minusSeconds(1).toEpochSecond();

		PanoramaPojo uploadData = new PanoramaPojo();
		uploadData.setCount(3);
		uploadData.setTimestamp(uploadTime);
		when(timestampToInstantHandler.convert(uploadTime)).thenReturn(Instant.ofEpochSecond(uploadTime));

		//when
		result = uploadService.uploadPanorama(uploadData, userZoneId);

		//then
		assertEquals("Status different from the expected", HttpStatus.NO_CONTENT, result);
	}

	@Test
	public void whenUploadedTimeEqualsCut_shouldUpload() {
		//given
		final long uploadTime = cut.toEpochSecond();

		PanoramaPojo uploadData = new PanoramaPojo();
		uploadData.setCount(3);
		uploadData.setTimestamp(uploadTime);
		when(timestampToInstantHandler.convert(uploadTime)).thenReturn(Instant.ofEpochSecond(uploadTime));

		//when
		result = uploadService.uploadPanorama(uploadData, userZoneId);

		//then
		assertEquals("Status different from the expected", HttpStatus.CREATED, result);
	}

	@Test
	public void whenUploadedTimeAfterCut_shouldUpload() {
		//given
		final long uploadTime = cut.plusSeconds(1).toEpochSecond();

		PanoramaPojo uploadData = new PanoramaPojo();
		uploadData.setCount(3);
		uploadData.setTimestamp(uploadTime);
		when(timestampToInstantHandler.convert(uploadTime)).thenReturn(Instant.ofEpochSecond(uploadTime));

		//when
		result = uploadService.uploadPanorama(uploadData, userZoneId);

		//then
		assertEquals("Status different from the expected", HttpStatus.CREATED, result);
	}

	@TestConfiguration
	static class UploadServiceImplTestContextConfiguration {

		@Bean
		public UploadService uploadService() {
			return new UploadServiceImpl();
		}
	}
}