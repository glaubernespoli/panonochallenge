package glaubernespoli.panono.code.challenge;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.OffsetDateTime;

import glaubernespoli.panono.code.challenge.pojo.PanoramaPojo;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.text.IsEmptyString.isEmptyString;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(
		webEnvironment = WebEnvironment.RANDOM_PORT,
		classes = Application.class)
@AutoConfigureMockMvc
@TestPropertySource(
		locations = "classpath:application-integrationtest.properties")
public class IntegratedApplicationTest {

	@Autowired
	private MockMvc mvc;

	@Test
	public void fullIntegratedTest() throws Exception {
		//given
		OffsetDateTime offsetDateTime = OffsetDateTime.now();

		//register 1 - ok
		PanoramaPojo uploadData = new PanoramaPojo();
		uploadData.setCount(4);
		uploadData.setTimestamp(offsetDateTime.toEpochSecond());

		upload(uploadData, CREATED.value());

		//register 2 - ok
		uploadData = new PanoramaPojo();
		uploadData.setCount(12);
		uploadData.setTimestamp(offsetDateTime.minusSeconds(5).toEpochSecond());

		upload(uploadData, CREATED.value());

		//register 3 - ok
		uploadData = new PanoramaPojo();
		uploadData.setCount(5);
		uploadData.setTimestamp(offsetDateTime.minusSeconds(9).toEpochSecond());

		upload(uploadData, CREATED.value());

		//register 4 - too old
		uploadData = new PanoramaPojo();
		uploadData.setCount(8);
		uploadData.setTimestamp(offsetDateTime.minusMinutes(2).toEpochSecond());

		upload(uploadData, NO_CONTENT.value());


		//then
		//statistics are around registers 1, 2 and 3
		mvc.perform(get("/statistics"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("sum", is(21)))
				.andExpect(jsonPath("avg", is(7.0)))
				.andExpect(jsonPath("max", is(12)))
				.andExpect(jsonPath("min", is(4)))
				.andExpect(jsonPath("count", is(3)));

	}

	private void upload(final PanoramaPojo uploadData, final int status) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson = ow.writeValueAsString(uploadData );

		mvc.perform(post("/upload")
				.contentType(APPLICATION_JSON)
				.content(requestJson))
				.andExpect(status().is(status))
				.andExpect(content().string(isEmptyString()));
	}
}