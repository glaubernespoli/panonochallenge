package glaubernespoli.panono.code.challenge.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import glaubernespoli.panono.code.challenge.pojo.PanoramaPojo;
import glaubernespoli.panono.code.challenge.pojo.Statistics;
import glaubernespoli.panono.code.challenge.service.StatisticsService;
import glaubernespoli.panono.code.challenge.service.UploadService;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.text.IsEmptyString.isEmptyString;
import static org.mockito.BDDMockito.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(MainController.class)
public class MainControllerTest {

	@Autowired
	private MockMvc mvc;

	@MockBean
	private UploadService uploadService;

	@MockBean
	private StatisticsService statisticsService;

	@Test
	public void givenValidPanoramaBatch_thenReturn201() throws Exception {
		PanoramaPojo uploadData = new PanoramaPojo();
		uploadData.setCount(3);
		uploadData.setTimestamp(123456L);

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson = ow.writeValueAsString(uploadData );

		given(uploadService.uploadPanorama(any(), any())).willReturn(HttpStatus.CREATED);

		mvc.perform(post("/upload")
			.contentType(APPLICATION_JSON)
			.content(requestJson))
			.andExpect(status().isCreated())
			.andExpect(content().string(isEmptyString()));

		verify(uploadService, times(1)).uploadPanorama(any(), any());
		verifyNoMoreInteractions(statisticsService);
	}

	@Test
	public void givenInvalidPanoramaBatch_thenReturn204() throws Exception {
		PanoramaPojo uploadData = new PanoramaPojo();
		uploadData.setCount(3);
		uploadData.setTimestamp(123456L);

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson = ow.writeValueAsString(uploadData );

		given(uploadService.uploadPanorama(any(), any())).willReturn(HttpStatus.NO_CONTENT);

		mvc.perform(post("/upload")
			.contentType(APPLICATION_JSON)
			.content(requestJson))
			.andExpect(status().isNoContent())
			.andExpect(content().string(isEmptyString()));

		verify(uploadService, times(1)).uploadPanorama(any(), any());
		verifyNoMoreInteractions(statisticsService);
	}

	@Test
	public void givenRequestToStatistics_thenReturn() throws Exception {
		Statistics statistics = new Statistics(21L, 7.0, 12, 4, 3L);

		given(statisticsService.getStatistics(any())).willReturn(statistics);

		mvc.perform(get("/statistics"))
			.andExpect(status().isOk())
			.andExpect(content().contentType(APPLICATION_JSON_UTF8))
			.andExpect(jsonPath("sum", is(21)))
			.andExpect(jsonPath("avg", is(7.0)))
			.andExpect(jsonPath("max", is(12)))
			.andExpect(jsonPath("min", is(4)))
			.andExpect(jsonPath("count", is(3)));

		verify(statisticsService, times(1)).getStatistics(any());
		verifyNoMoreInteractions(statisticsService);
	}
}
