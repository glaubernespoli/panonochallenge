package glaubernespoli.panono.code.challenge.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.OffsetDateTime;
import java.time.ZoneId;

import glaubernespoli.panono.code.challenge.entity.Panorama;
import glaubernespoli.panono.code.challenge.pojo.Statistics;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PanoramaRepositoryTest {

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private PanoramaRepository panoramaRepository;

	private OffsetDateTime now;

	private OffsetDateTime cut;

	@Before
	public void init() {
		now = OffsetDateTime.now(ZoneId.of("UTC"));
	}


	@Test
	public void whenInCutRange_thenReturnStatisticWith1() {
		//given
		Panorama panorama = new Panorama();
		panorama.setCount(5);
		panorama.setCreatedAt(now);

		entityManager.persistAndFlush(panorama);

		cut = now.minusSeconds(5);

		//when
		Statistics statistics = panoramaRepository.findByCreatedAtGreaterThan(cut);

		//then
		assertEquals("Sum different from expected", new Long(5), statistics.getSum());
		assertEquals("Avg different from expected", new Double(5.0), statistics.getAvg());
		assertEquals("Max different from expected", new Integer(5), statistics.getMax());
		assertEquals("Min different from expected", new Integer(5), statistics.getMin());
		assertEquals("Count different from expected.", new Long(1), statistics.getCount());
	}

	@Test
	public void whenInCutRange_thenReturnStatisticWith3() {
		//given
		Panorama panorama = new Panorama();
		panorama.setCount(5);
		panorama.setCreatedAt(now);
		entityManager.persistAndFlush(panorama);

		panorama = new Panorama();
		panorama.setCount(2);
		panorama.setCreatedAt(now);
		entityManager.persistAndFlush(panorama);

		panorama = new Panorama();
		panorama.setCount(12);
		panorama.setCreatedAt(now);
		entityManager.persistAndFlush(panorama);

		cut = now.minusSeconds(5);

		//when
		Statistics statistics = panoramaRepository.findByCreatedAtGreaterThan(cut);

		//then
		assertEquals("Sum different from expected", new Long(19), statistics.getSum());
		assertEquals("Avg different from expected", new Double(6.3), statistics.getAvg());
		assertEquals("Max different from expected", new Integer(12), statistics.getMax());
		assertEquals("Min different from expected", new Integer(2), statistics.getMin());
		assertEquals("Count different from expected.", new Long(3), statistics.getCount());
	}

	@Test
	public void whenOutOfCutRange_thenReturnStatisticWithNullValues() {
		//given
		Panorama panorama = new Panorama();
		panorama.setCount(5);
		panorama.setCreatedAt(now);

		entityManager.persistAndFlush(panorama);

		cut = now.plusSeconds(1);

		//when
		Statistics statistics = panoramaRepository.findByCreatedAtGreaterThan(cut);

		//then
		assertEquals("Sum different from expected", new Long(0), statistics.getSum());
		assertEquals("Avg different from expected", new Double(0.0), statistics.getAvg());
		assertEquals("Max different from expected", new Integer(0), statistics.getMax());
		assertEquals("Min different from expected", new Integer(0), statistics.getMin());
		assertEquals("Count different from expected.", new Long(0), statistics.getCount());
	}

	@Test
	public void whenInCutRangeWithDifferentZoneId_thenReturnStatisticWith1() {
		//given
		now = OffsetDateTime.now(ZoneId.of("Brazil/East"));

		Panorama panorama = new Panorama();
		panorama.setCount(5);
		panorama.setCreatedAt(now);

		entityManager.persistAndFlush(panorama);

		cut = now.minusSeconds(5);

		//when
		Statistics statistics = panoramaRepository.findByCreatedAtGreaterThan(cut);

		//then
		assertEquals("Sum different from expected", new Long(5), statistics.getSum());
		assertEquals("Avg different from expected", new Double(5.0), statistics.getAvg());
		assertEquals("Max different from expected", new Integer(5), statistics.getMax());
		assertEquals("Min different from expected", new Integer(5), statistics.getMin());
		assertEquals("Count different from expected.", new Long(1), statistics.getCount());
	}
}