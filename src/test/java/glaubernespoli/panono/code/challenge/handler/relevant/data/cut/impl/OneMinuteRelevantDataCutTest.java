package glaubernespoli.panono.code.challenge.handler.relevant.data.cut.impl;


import org.junit.BeforeClass;
import org.junit.Test;

import java.time.OffsetDateTime;
import java.time.ZoneId;

import glaubernespoli.panono.code.challenge.handler.relevant.data.cut.RelevantDataCutHandler;

import static org.junit.Assert.assertEquals;

/**
 * NOTE: This test might fail in a very rare case. Most Cases both dates will be taken in the same millisec, but since
 * it's slightly common that the {@link OffsetDateTime#now(ZoneId)} used in the assert is in the next millisecond, I
 * put the comparison to be done with no nanosec. The only time an error might occur is the case where the cut
 * handler returns a date in the last millisecond of the second, and then the date taken at the assert comparison
 * goes to the next millisecond, which implies in changing to the next second. It's a super rare fail case though,
 * and shouldn't happen in a lifetime :)
 */
public class OneMinuteRelevantDataCutTest {

	private static RelevantDataCutHandler cutHandler;

	@BeforeClass
	public static void initOnce() {
		cutHandler = new OneMinuteRelevantDataCut();
	}

	@Test
	public void testCutResultUTC() {
		final ZoneId utc = ZoneId.of("UTC");
		OffsetDateTime oneMinuteCut = cutHandler.getCut(utc);
		assertEquals("Time is different.", OffsetDateTime.now(utc).withNano(0), oneMinuteCut.plusMinutes(1).withNano
				(0));
	}

}