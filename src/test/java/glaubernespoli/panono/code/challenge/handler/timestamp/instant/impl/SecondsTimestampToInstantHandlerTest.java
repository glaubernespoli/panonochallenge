package glaubernespoli.panono.code.challenge.handler.timestamp.instant.impl;

import org.junit.BeforeClass;
import org.junit.Test;

import java.time.Instant;

import glaubernespoli.panono.code.challenge.handler.timestamp.instant.TimestampToInstantHandler;

import static org.junit.Assert.assertEquals;

public class SecondsTimestampToInstantHandlerTest {

	private static TimestampToInstantHandler handler;

	private static long timestamp;

	@BeforeClass
	public static void initOnce() {
		handler = new SecondsTimestampToInstantHandler();
		timestamp = 1234567890;
	}

	@Test
	public void convert() {
		Instant result = handler.convert(timestamp);
		assertEquals("Timestamp different than spected", timestamp, result.getEpochSecond());
	}

}