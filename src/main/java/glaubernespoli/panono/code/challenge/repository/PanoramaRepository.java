package glaubernespoli.panono.code.challenge.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.OffsetDateTime;

import glaubernespoli.panono.code.challenge.entity.Panorama;
import glaubernespoli.panono.code.challenge.pojo.Statistics;

/**
 * Repository for {@link Panorama}.
 *
 * @author glaubernespoli
 */
@Repository
public interface PanoramaRepository extends CrudRepository<Panorama, Long> {

	String GET_STATISTICS_QUERY = "select new glaubernespoli.panono.code.challenge.pojo.Statistics(" +
			"sum(p.count), " +
			"avg(p.count), " +
			"max(p.count), " +
			"min(p.count), " +
			"count(p) " +
			") " +
			"from Panorama p " +
			"where p.createdAt >= :cut ";

	/**
	 * Returns a {@link Statistics} from the batch uploads that are greater than the cut parameter.
	 *
	 * @param cut the cut
	 *
	 * @return a {@link Statistics}
	 */
	@Query(value = GET_STATISTICS_QUERY)
	Statistics findByCreatedAtGreaterThan(@Param("cut") OffsetDateTime cut);
}
