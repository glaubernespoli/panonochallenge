package glaubernespoli.panono.code.challenge.entity;

import java.time.OffsetDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Entity responsible for holding the information of a batch upload.
 *
 * @author glaubernespoli
 */
@Entity
@Table(indexes = {@Index(name = "COUNTIX", columnList = "count"), @Index(name = "CREATDIX", columnList = "created_at")})
public class Panorama {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NotNull
	@Min(value = 1, message = "Can't upload less than 1 panoramas at once.")
	@Column(name = "count", nullable = false, updatable = false)
	private Integer count;

	@NotNull
	@Column(name = "created_at", nullable = false, updatable = false)
	private OffsetDateTime createdAt;

	/**
	 * C'tor
	 */
	public Panorama() {
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id
	 */
	public void setId(final Long id) {
		this.id = id;
	}

	/**
	 * @return the count
	 */
	public Integer getCount() {
		return count;
	}

	/**
	 * @param count the count
	 */
	public void setCount(final Integer count) {
		this.count = count;
	}

	/**
	 * @return the date and time it was created
	 */
	public OffsetDateTime getCreatedAt() {
		return createdAt;
	}

	/**
	 * @param createdAt the date and time it was created
	 */
	public void setCreatedAt(final OffsetDateTime createdAt) {
		this.createdAt = createdAt;
	}
}
