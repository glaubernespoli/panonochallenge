package glaubernespoli.panono.code.challenge.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.ZoneId;

import javax.validation.Valid;

import glaubernespoli.panono.code.challenge.pojo.PanoramaPojo;
import glaubernespoli.panono.code.challenge.pojo.Statistics;
import glaubernespoli.panono.code.challenge.service.StatisticsService;
import glaubernespoli.panono.code.challenge.service.UploadService;

/**
 * Main application controller. Holds both endpoints needed for this application.
 *
 * @author glaubernespoli
 */
@RestController
public class MainController {

	@Autowired
	private UploadService uploadService;

	@Autowired
	private StatisticsService statisticsService;

	/**
	 * Endpoint used to upload a batch of panoramas.
	 *
	 * @param uploadData the {@link PanoramaPojo} containing the batch upload data.
	 * @param userZoneId the {@link ZoneId} from the user who uploaded the batch.
	 *
	 * @return an empty {@link ResponseEntity} with either a 201 code (success) or 204 (if the uploaded batch is
	 * older than 60 seconds).
	 */
	@ResponseBody
	@PostMapping(value = "/upload", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity uploadPanoramas(@RequestBody @Valid PanoramaPojo uploadData, ZoneId userZoneId) {
		return new ResponseEntity(uploadService.uploadPanorama(uploadData, userZoneId));
	}

	/**
	 * Endpoint used to get statistics about the uploads done in the last 60 seconds.
	 *
	 * @param userZoneId the {@link ZoneId} from the user who did the request.
	 *
	 * @return a {@link Statistics} containing the information about the uploads done in the last 60 seconds.
	 */
	@ResponseBody
	@GetMapping(value = "/statistics", produces = MediaType.APPLICATION_JSON_VALUE)
	public Statistics getStatistics(ZoneId userZoneId) {
		return statisticsService.getStatistics(userZoneId);
	}
}
