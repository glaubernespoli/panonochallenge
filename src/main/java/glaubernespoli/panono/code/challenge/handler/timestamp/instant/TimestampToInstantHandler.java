package glaubernespoli.panono.code.challenge.handler.timestamp.instant;

import java.time.Instant;

/**
 * Contract that defines how a timestamp should be converted to an {@link Instant}.
 *
 * @author glaubernespoli
 */
public interface TimestampToInstantHandler {

	/**
	 * Receives a timestamp as a {@link Long} and converts it to an {@link Instant}.
	 * @param timestamp the timestamp
	 * @return an {@link Instant}
	 */
	Instant convert(final long timestamp);
}
