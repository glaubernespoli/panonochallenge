package glaubernespoli.panono.code.challenge.handler.relevant.data.cut.impl;

import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;
import java.time.ZoneId;

import glaubernespoli.panono.code.challenge.handler.relevant.data.cut.RelevantDataCutHandler;


/**
 * Implementation from {@link RelevantDataCutHandler} that implies that the relevant data  is within one minute.
 *
 * @author glaubernespoli
 */
@Component
public class OneMinuteRelevantDataCut implements RelevantDataCutHandler {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public OffsetDateTime getCut(final ZoneId userZoneId) {
		return OffsetDateTime.now(userZoneId).minusMinutes(1);
	}
}
