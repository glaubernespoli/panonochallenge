package glaubernespoli.panono.code.challenge.handler.relevant.data.cut;

import java.time.OffsetDateTime;
import java.time.ZoneId;

/**
 * Contract interface that defines which period of time the data starts to be relevant to the system.
 *
 * @author glaubernespoli
 */
public interface RelevantDataCutHandler {

	/**
	 * Gets an {@link OffsetDateTime} that marks the cut where the data starts to be important to the system.
	 *
	 * @param userZoneId the {@link ZoneId} from the user
	 *
	 * @return an {@link OffsetDateTime}
	 */
	OffsetDateTime getCut(final ZoneId userZoneId);
}
