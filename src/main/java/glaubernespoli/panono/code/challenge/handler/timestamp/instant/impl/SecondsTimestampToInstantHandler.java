package glaubernespoli.panono.code.challenge.handler.timestamp.instant.impl;

import org.springframework.stereotype.Component;

import java.time.Instant;

import glaubernespoli.panono.code.challenge.handler.timestamp.instant.TimestampToInstantHandler;

/**
 * Implementation of {@link TimestampToInstantHandler} that defines the timestamp received is in seconds from the
 * Epoch time.
 *
 * @author glaubernespoli
 */
@Component
public class SecondsTimestampToInstantHandler implements TimestampToInstantHandler {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Instant convert(final long timestamp) {
		return Instant.ofEpochSecond(timestamp);
	}
}
