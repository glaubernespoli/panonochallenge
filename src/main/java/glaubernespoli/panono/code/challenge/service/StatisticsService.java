package glaubernespoli.panono.code.challenge.service;

import java.time.ZoneId;

import glaubernespoli.panono.code.challenge.pojo.Statistics;

/**
 * Service responsible for retrieving a @{@link Statistics}.
 *
 * @author glaubernespoli
 */
public interface StatisticsService {

	/**
	 * Returns the upload statistics from the user, based on his {@link ZoneId} and the relevant data cut.
	 *
	 * @param userZoneId the user {@link ZoneId}
	 *
	 * @return a {@link Statistics} from the uploads
	 */
	Statistics getStatistics(final ZoneId userZoneId);

}
