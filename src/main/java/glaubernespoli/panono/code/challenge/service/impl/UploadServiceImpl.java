package glaubernespoli.panono.code.challenge.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.time.ZoneId;

import glaubernespoli.panono.code.challenge.handler.relevant.data.cut.RelevantDataCutHandler;
import glaubernespoli.panono.code.challenge.handler.timestamp.instant.TimestampToInstantHandler;
import glaubernespoli.panono.code.challenge.pojo.PanoramaPojo;
import glaubernespoli.panono.code.challenge.repository.PanoramaRepository;
import glaubernespoli.panono.code.challenge.service.UploadService;

/**
 * Default implementation of {@link UploadService}.
 *
 * @author glaubernespoli
 */
@Service
public class UploadServiceImpl implements UploadService {

	@Autowired
	private PanoramaRepository panoramaRepository;

	@Autowired
	private RelevantDataCutHandler relevantDataCutHandler;

	@Autowired
	private TimestampToInstantHandler timestampToInstantHandler;

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	public HttpStatus uploadPanorama(final PanoramaPojo uploadData, final ZoneId userZoneId) {
		if(shouldUpload(uploadData, userZoneId)) {
			panoramaRepository.save(uploadData.toEntity(getUploadedTime(uploadData.getTimestamp(), userZoneId)));
			return HttpStatus.CREATED;
		}
		return HttpStatus.NO_CONTENT;

	}


	/**
	 * {@inheritDoc}
	 */
	public boolean shouldUpload(final PanoramaPojo uploadData, final ZoneId userZoneId) {
		OffsetDateTime cut = relevantDataCutHandler.getCut(userZoneId);
		OffsetDateTime uploadedTime = getUploadedTime(uploadData.getTimestamp(), userZoneId);
		return uploadedTime.isAfter(cut) || uploadedTime.isEqual(cut);
	}

	/**
	 * Returns an {@link OffsetDateTime} of which the batch was uploaded.
	 *
	 * @param timestamp  the timestamp of which the batch was uploaded in the Epoch time.
	 * @param userZoneId the zone id from the user
	 *
	 * @return an {@link OffsetDateTime}
	 */
	private OffsetDateTime getUploadedTime(final Long timestamp, final ZoneId userZoneId) {
		return OffsetDateTime.ofInstant(timestampToInstantHandler.convert(timestamp), userZoneId);
	}
}
