package glaubernespoli.panono.code.challenge.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.time.ZoneId;

import glaubernespoli.panono.code.challenge.handler.relevant.data.cut.RelevantDataCutHandler;
import glaubernespoli.panono.code.challenge.pojo.Statistics;
import glaubernespoli.panono.code.challenge.repository.PanoramaRepository;
import glaubernespoli.panono.code.challenge.service.StatisticsService;

/**
 * Main implementation of {@link StatisticsService}.
 *
 * @author glaubernespoli
 */
@Service
@Transactional(readOnly = true)
public class StatisticsServiceImpl implements StatisticsService {

	@Autowired
	private PanoramaRepository panoramaRepository;

	@Autowired
	private RelevantDataCutHandler relevantDataCutHandler;

	/**
	 * {@inheritDoc}
	 */
	public Statistics getStatistics(final ZoneId userZoneId) {
		OffsetDateTime cut = relevantDataCutHandler.getCut(userZoneId);
		return panoramaRepository.findByCreatedAtGreaterThan(cut);
	}
}
