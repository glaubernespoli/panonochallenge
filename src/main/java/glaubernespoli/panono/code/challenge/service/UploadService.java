package glaubernespoli.panono.code.challenge.service;

import org.springframework.http.HttpStatus;

import java.time.ZoneId;

import glaubernespoli.panono.code.challenge.pojo.PanoramaPojo;

/**
 * Service responsible for the batch uploads.
 *
 * @author glaubernespoli
 */
public interface UploadService {

	/**
	 * Uploads a batch of panoramas to the database, and returns a {@link HttpStatus} based on the successfulness of
	 * the upload. The successfulness is based on the relevant data cut used by the system. If the time the upload was
	 * done is within the period the data is relevant to the system, then the bacth will be uploaded. Otherwise, it'll
	 * be discarded.
	 *
	 * @param uploadData the batch upload data
	 * @param userZoneId the zone id from the user
	 *
	 * @return an {@link HttpStatus#CREATED} if successfully uploaded, otherwise returns a
	 * {@link HttpStatus#NO_CONTENT}.
	 */
	HttpStatus uploadPanorama(final PanoramaPojo uploadData, final ZoneId userZoneId);

	/**
	 * Verifies if the newly uploaded batch is apt to be actually uploaded. It will if, and only if, the time the
	 * batch was uploaded is within the relevant data cut of the system.
	 *
	 * @param uploadData the newly uploaded data
	 * @param userZoneId the zone id from the user
	 *
	 * @return TRUE if the relevant data cut is before the time the batch was uploaded; FALSE otherwise.
	 */
	boolean shouldUpload(final PanoramaPojo uploadData, final ZoneId userZoneId);
}
