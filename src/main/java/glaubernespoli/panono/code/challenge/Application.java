package glaubernespoli.panono.code.challenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Application class, responsible for running the app.
 *
 * @author glaubernespoli
 */
@SpringBootApplication
public class Application {

	/**
	 * Main method.
	 *
	 * @param args args
	 */
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
