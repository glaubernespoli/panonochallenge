package glaubernespoli.panono.code.challenge.pojo;

import java.time.OffsetDateTime;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import glaubernespoli.panono.code.challenge.entity.Panorama;

/**
 * POJO responsible for holding data relative to a {@link Panorama} batch upload.
 *
 * @author glaubernespoli
 */
public class PanoramaPojo {

	@NotNull
	@Min(value = 1, message = "Can't upload less than 1 panoramas at once.")
	private Integer count;

	@NotNull
	private Long timestamp;

	/**
	 * C'tor
	 */
	public PanoramaPojo() {
	}

	/**
	 * @return the count
	 */
	public Integer getCount() {
		return count;
	}

	/**
	 * @param count the count
	 */
	public void setCount(final Integer count) {
		this.count = count;
	}

	/**
	 * @return the timestamp
	 */
	public Long getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp the timestamp
	 */
	public void setTimestamp(final Long timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * Converts the POJO to a {@link Panorama} entity.
	 *
	 * @param createdAt the {@link OffsetDateTime} of which this batch was uploaded.
	 *
	 * @return a {@link Panorama} containing the data related to the uploaded batch.
	 */
	public Panorama toEntity(OffsetDateTime createdAt) {
		Panorama panorama = new Panorama();
		panorama.setCount(getCount());
		panorama.setCreatedAt(createdAt);
		return panorama;
	}
}
