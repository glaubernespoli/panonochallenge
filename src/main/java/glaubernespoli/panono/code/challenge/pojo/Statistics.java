package glaubernespoli.panono.code.challenge.pojo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * POJO responsible for holding the statistics about the batch uploads done by the user in the last period.
 *
 * @author glaubernespoli
 */
public class Statistics implements Serializable {

	private final Long sum;

	private final Double avg;

	private final Integer max;

	private final Integer min;

	private final Long count;

	/**
	 * C'tor
	 *
	 * @param sum   total amount of uploaded panoramas
	 * @param avg   average amount of uploaded panoramas per batch
	 * @param max   max amount of uploaded panoramas per batch
	 * @param min   min amount of uploaded panoramas per batch
	 * @param count total of batch uploads
	 */
	public Statistics(final Long sum, final Double avg, final Integer max, final Integer min, final Long count) {
		this.sum = sum != null ? sum : 0;
		this.avg = avg != null ? BigDecimal.valueOf(avg).setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue() : 0.0;
		this.max = max != null ? max : 0;
		this.min = min != null ? min : 0;
		this.count = count != null ? count : 0;
	}

	/**
	 * @return the sum
	 */
	public Long getSum() {
		return sum;
	}

	/**
	 * @return the avg
	 */
	public Double getAvg() {
		return avg;
	}

	/**
	 * @return the max
	 */
	public Integer getMax() {
		return max;
	}

	/**
	 * @return the min
	 */
	public Integer getMin() {
		return min;
	}

	/**
	 * @return the count
	 */
	public Long getCount() {
		return count;
	}
}
